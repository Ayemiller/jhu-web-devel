<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.millera.model.beans.*"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	    <title>Beartooth Hiking Company (BHC)</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	    <meta property="og:url" content="https://web7.jhuep.com/millera_proj11/cost.jsp">
	    <meta property="og:type" content="website">
	    <meta property="og:title" content="Beartooth Hiking Company">
	    <meta property="og:description" content="Learn about hiking with BHC.">
	    <meta property="og:image" content="https://web7.jhuep.com/millera_proj11/images/unsplash-peak.jpg">
	    <meta property="og:image:type" content="image/jpg">
	    <meta property="og:image:width" content="1200">
	    <meta property="og:image:height" content="630">
	    <link rel="stylesheet" type="text/css" href="style.css">
	    <jsp:useBean id="startdate" class="com.millera.model.beans.StartDate" scope="session" />
	    <jsp:setProperty property="*" name="startdate" />
	</head>
  
	<body>
		<header id="header">
			<img id="logo" src="images/logo2.png" alt="Beartooth Hiking Company">
		</header>
		<main>
		<%
		ReservationQuery query = new ReservationQuery(); 
		List<Reservation> reservations = query.executeQuery(startdate.getReservationStartDate());
		out.println("\t\t<h3>" + reservations.size() + " Reservations begin on or after " + startdate.getReservationStartDate() + "</h3>");
		%>
		<div style="overflow-x: auto;">
		<table class="pricing">
			<tr>
				<th>Start Day</th>
	    		<th>End Day</th>
        		<th>Location</th>
        		<th>Guide</th>
        		<th>Reservation</th>
        	</tr>
		<%
		for(Reservation reservation : reservations) { 
		  	out.println("\t\t\t<tr>");
			out.println("\t\t\t\t<td>" + reservation.getReservationStartDate() + "</td>");
			out.println("\t\t\t\t<td>" + reservation.getReservationEndDate() + "</td>");
			out.println("\t\t\t\t<td>" + reservation.getLocation() + "</td>");
			out.println("\t\t\t\t<td>" + reservation.getGuideFirstName() + ' ' + reservation.getGuideLastName() + "</td>");
			out.println("\t\t\t\t<td>" + reservation.getReservationFirstName() + ' '  + reservation.getReservationLastName() + "</td>");
		  	out.println("\t\t\t</tr>");
        }
		%>
		</table>
		</div>
		<a href="logout.jsp">...go back</a>
    	</main>
	</body>
</html>
