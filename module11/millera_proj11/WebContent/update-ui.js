jQuery(document).ready(function() {
	  
	  $('#datepicker').datepicker({
		  dateFormat: "yy-mm-dd",
		  minDate: new Date('2007-6-1'),
		  maxDate: new Date('2040-9-29'),
		  changeMonth: true,
		  changeYear: true,
		 });
	  $('#datepicker').datepicker("setDate", new Date(2021,5,1) );

});