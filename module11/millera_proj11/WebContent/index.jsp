<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="update-ui.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Beartooth Hiking Company (BHC)</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="https://web7.jhuep.com/millera_proj11/index.jsp">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Beartooth Hiking Company">
    <meta property="og:description" content="Learn about hiking with BHC.">
    <meta property="og:image" content="https://web7.jhuep.com/millera_proj11/images/unsplash-peak.jpg">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  
  <body>
    <header id="header">
      <img id="logo" src="images/logo2.png" alt="Beartooth Hiking Company">
    </header>
		<main>
			<p>
		  		<h2>Beartooth Hiking Reservation Finder</h2>
		        <form action="reservations.jsp" method=POST>
			        <table>
						<tr>
							<td>
								Locate reservations that started on or before:
							</td>
							<td>
								<input type="text" name="reservationStartDate" id="datepicker" readonly="readonly">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input type="SUBMIT">
							</td>
						</tr>
					</table>
				</form>
			</p>
    </main>
  </body>
</html>
