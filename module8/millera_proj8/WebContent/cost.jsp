<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Beartooth Hiking Company (BHC)</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:url" content="https://web7.jhuep.com/millera_proj7/cost.jsp">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Beartooth Hiking Company">
    <meta property="og:description" content="Learn about hiking with BHC.">
    <meta property="og:image" content="https://web7.jhuep.com/millera_proj7/images/unsplash-peak.jpg">
    <meta property="og:image:type" content="image/jpg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <link rel="stylesheet" type="text/css" href="style.css">
    <jsp:useBean id="hikeInfo" class="com.millera.model.beans.HikeInfo" scope="session" />
  </head>
  
  <body>
    <header id="header">
      <img id="logo" src="images/logo2.png" alt="Beartooth Hiking Company">
    </header>
		<main>
				<h3>Your Trip Estimate</h1>
				<h4>Trail: ${hikeInfo.hike}</h2>
				<h4>Dates: ${hikeInfo.beginDate} to ${hikeInfo.endDate}</h2>
				<h4>Estimated Trip Cost: ${hikeInfo.costString}</h2>
				<% if(Integer.parseInt(hikeInfo.getNormalDays()) > 0) { %>
					<h5>${hikeInfo.normalDays} days @ ${hikeInfo.baseRate} per day</h3>
			    <% } %>
				<% if(Integer.parseInt(hikeInfo.getPremiumDays()) > 0) { %>
					<h5>${hikeInfo.premiumDays} days @ ${hikeInfo.premiumRate} per day</h3>
			    <% } %>
				<a href="logout.jsp">...go back</a>
    </main>
  </body>
</html>
