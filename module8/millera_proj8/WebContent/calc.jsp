  <jsp:useBean id="hikeInfo" class="com.millera.model.beans.HikeInfo" scope="session" />
  <%@ page import="com.millera.model.Rates" %>
  <%@ page import="com.millera.model.beans.HikeInfo" %>

  <h2>Beartooth Hiking Trip Calculator</h2>
  
  <h6>(The hiking season is from June 1 to October 1)</h6>
        <% if (hikeInfo.getMessage() != null) { %>
        	<span style="color: red"><%=hikeInfo.getMessage() %></span><br />
        <% } %>
        <form action="Controller" method=POST>
        <table>
			<tr>
				<td>
					Starting Date:
				</td>
				<td>
					<input type="text" name="date" id="datepicker" readonly="readonly">
				</td>
			</tr>
			<tr>
				<td>
					Possible Hikes:
				</td>
				<td>
					<select name="hike" id="hike">
					<option value="BEATEN" selected>BEATEN</option>
					<option value="HELLROARING">HELLROARING</option>
					<option value="GARDINER">GARDINER</option>
					<!-- <% for(Rates.HIKE hikeName : Rates.HIKE.values()) { %>
						<option value="<%=hikeName %>"><%=hikeName %></option>
					<% } %>-->
            		</select>
				</td>
			</tr>
			<tr>
				<td>
					Hike Duration:
				</td>
				<td>
					<select name="duration" id="duration">
					<% for(int numDays : HikeInfo.VALID_DURATIONS) { %>
						<option value="<%=numDays %>"><%=numDays %></option>
					<% } %>
            		</select>
				</td>
			</tr>
			<tr>
				<td>
					Party Size:
				</td>
				<td>
					<select name="partySize">
					<% for(int numInParty=1; numInParty<=HikeInfo.MAX_PARTY_SIZE; numInParty++) { %>
						<option value="<%=numInParty %>"><%=numInParty %></option>
					<% } %>
            		</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="SUBMIT">
				</td>
			</tr>
		</table>
		</form>
