  jQuery(document).ready(function() {
	  function disableSpecificWeekDays(date) {

		  var day = date.getDate();
		  var month = date.getMonth();
		  var duration = document.getElementById('duration').value;
		  var monthsToDisable = [0,1,2,3,4,9,10,11];
		  // disable certain days in September if the duration would exceed Oct 1
		  if (month==8 && day+parseInt(duration) > 32) {
	          return [false];
	      }
		    
	      // disable Jan-May, Oct-Dec
	      return [$.inArray(month, monthsToDisable) == -1];
	  }
	  
	  $('#datepicker').datepicker({
		  dateFormat: "yy-mm-dd",
		  minDate: new Date('2007-6-1'),
		  maxDate: new Date('2040-9-29'),
		  beforeShowDay: disableSpecificWeekDays,
		  changeMonth: true,
		  changeYear: true,
		 });
	  $('#datepicker').datepicker("setDate", new Date(2021,5,1) );

      } );

      jQuery(document).ready(function() {
    	function updateDuration(values) {
	    $("#duration").empty();
	    values.forEach((v,i) => $("#duration").append($("<option>").val(v).text(v)));
    	}
	  	$("#hike").change(function() {
	    var v = $(this).val();
	    if (v == "BEATEN") {
	    	updateDuration(['5','7']);
	    } else if (v == "GARDINER") {
	    	updateDuration(['3','5']);
	    } else {
	    	updateDuration(['2','3','4']);
	    }
	  });
	  $("#hike").change();
    });