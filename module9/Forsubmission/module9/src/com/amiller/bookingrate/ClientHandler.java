package com.amiller.bookingrate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * This class implements a Runnable that is executed by our Server class. It reads the information
 * sent from the client, processes it, and sends a response to the client, on its own thread.
 */
public class ClientHandler implements Runnable {

  private static final String DOLLAR_FORMAT = "%.2f";
  private static final Logger LOGGER = Logger.getLogger(ClientHandler.class.getName());
  private static final int NUMBER_OF_INPUT_FIELDS = 5; // max number of input fields for error checking
  private static final String INCORRECT_COST = "-0.01";
  private static final String NOT_ENOUGH_TOKENS = "Not enough input tokens";
  private static final String EMPTY_OR_NULL_INPUT = "Empty or null input from client";
  private static final String REPLY_FORMAT = "Sending reply of cost %s with message: \"%s\"";
  private final Socket client; // the client socket

  /**
   * Constructor for our ClientHandler runnable
   *
   * @param client - the client Socket
   */
  ClientHandler(final Socket client) {
    this.client = client;
  }

  /**
   * Method to send a reply to the client containing the cost and an informative message.
   *
   * @param out     - BufferedWriter for output
   * @param cost    - The estimated cost of the trip
   * @param message - The message to provide in the response
   * @throws IOException
   */
  private static void sendReply(final BufferedWriter out, final String cost, String message)
      throws IOException {

    /* If the cost is greater than zero, we set our message to "Quoted Rate" to indicate a valid quote */
    if (Double.parseDouble(cost) > 0) {
      message = "Quoted Rate";
    }
    /* Log the response data that we're sending */
    ClientHandler.LOGGER.info(String
        .format(ClientHandler.REPLY_FORMAT, cost,
            message));

    /* Format the response String */
    final String rawReply = String.format("%s:%s", cost, message);

    /* Log the raw data that is being sent to the client */
    ClientHandler.LOGGER.info(String.format("Raw reply: %s\n", rawReply));
    out.write(rawReply);
  }

  @Override
  public void run() {
    Rates rate = null;

    /* Setup input and output streams from the client */
    try (
        final BufferedReader in = new BufferedReader(
            new InputStreamReader(this.client.getInputStream()));
        final BufferedWriter out = new BufferedWriter(
            new OutputStreamWriter(this.client.getOutputStream()))) {

      /* Retrieve the client's request */
      final String rawInput = in.readLine();
      final String[] clientInput;

      /* If client's request is null or empty, provide a response right away */
      if (rawInput == null || "".equals(rawInput)) {
        ClientHandler
            .sendReply(out, ClientHandler.INCORRECT_COST, ClientHandler.EMPTY_OR_NULL_INPUT);
      } else {
        /* Log the client's raw request */
        ClientHandler.LOGGER.info(String.format("Raw request: %s", rawInput));

        /* Tokenize the client's request */
        clientInput = rawInput.split(":");

        /* If client's request does not equal the required number of fields, provide a response right away */
        if (clientInput.length != ClientHandler.NUMBER_OF_INPUT_FIELDS) {
          ClientHandler
              .sendReply(out, ClientHandler.INCORRECT_COST, ClientHandler.NOT_ENOUGH_TOKENS);
        } else {

          /* Set the request fields from the client's request */
          final String hikeText = clientInput[0];
          final String yearText = clientInput[1];
          final String monthText = clientInput[2];
          final String dayText = clientInput[3];
          final String durationText = clientInput[4];

          /* Convert the request fields from Strings into the appropriate types */
          final Rates.HIKE chosenHike = Rates.HIKE.values()[Integer.parseInt(hikeText)];
          final Integer year = Integer.parseInt(yearText);
          final Integer month = Integer.parseInt(monthText);
          final Integer day = Integer.parseInt(dayText);
          final Integer duration = Integer.parseInt(durationText);

          /* Calculate the Rate using BookingDay and Rates classes */
          rate = new Rates(chosenHike);
          final BookingDay beginDate = new BookingDay(year, month, day);
          rate.setBeginDate(beginDate);
          rate.setDuration(duration);

          /* Log the client's request information */
          ClientHandler.LOGGER.info(String
              .format(
                  "Received request for hike \"%s\" starting %s/%s/%s for a duration of %s days.",
                  chosenHike.toString(), monthText, dayText, year, durationText));

          /* Store the cost and the details */
          final double cost = rate.getCost();
          final String formattedCost = String.format(ClientHandler.DOLLAR_FORMAT, cost);
          final String details = rate.getDetails();

          /* Send the reply to the client */
          ClientHandler.sendReply(out, formattedCost, details);
        }
      }
    } catch (final IOException e) {
      /* Try to send an error message reply if possible */
      ClientHandler.LOGGER.severe("Error handled while attempting to read socket");
      try (final BufferedWriter out = new BufferedWriter(
          new OutputStreamWriter(this.client.getOutputStream()))) {
        if (rate != null) {
          // print out the error message from the Rates class
          ClientHandler.sendReply(out, ClientHandler.INCORRECT_COST, rate.getDetails());
        } else {
          // otherwise provide the exception message
          ClientHandler.sendReply(out, ClientHandler.INCORRECT_COST, e.getMessage());
        }
      } catch (final IOException e1) {
        /* We failed trying to respond to the client, just fail and print stack trace */
        ClientHandler.LOGGER.severe("Unable to send a response!");
        e1.printStackTrace();
      }
    }
  }

}
