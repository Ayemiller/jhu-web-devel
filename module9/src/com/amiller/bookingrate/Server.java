package com.amiller.bookingrate;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class creates our multi-threaded server that will listen for requests from clients and issue
 * responses based on BHC rates. It handles all invalid input and provides informative messages to
 * the client.
 */
public class Server {

  private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
  private static final int PORT = 20012;

  public static void main(final String[] args) {
    /* Set Logger format [YYYY-MM-DD HH:MM:SS] [<Log Level>] Message */
    System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");

    /* Create a thread pool of 10 * the number of available processors */
    final int poolSize = 10 * Runtime.getRuntime().availableProcessors();
    final ExecutorService executor = Executors.newFixedThreadPool(poolSize);

    /* Create the ServerSocket to accept the client's connection. */
    final ServerSocket server;
    try {
      server = new ServerSocket(Server.PORT);
      Server.LOGGER
          .log(Level.INFO, () -> String.format("Created server on port %s\n", Server.PORT));
      while (true) {
        try {
          /* Issue a blocking call to accept, and execute ClientHandler Runnable on its own thread */
          final Socket client = server.accept();
          executor.execute(new ClientHandler(client));
        } catch (final IOException clientException) {
          Server.LOGGER.severe(String
              .format("Exception handled trying to accept client socket.\n%s",
                  clientException.getStackTrace()));
        }
      }
    } catch (final IOException serverException) {
      /* If this exception is reached, the server has crashed */
      Server.LOGGER.severe(
          String.format(String.format("Server crashed.\n%s", serverException.getStackTrace())));
      executor.shutdownNow();
    }
  }

}
