package jhu.webdevel.proj4;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class that tests the classes created in project 4
 *
 * @author Adam Miller (amill189)
 *
 */
public class TestProject4 {
  public static void main(final String[] args) {
    // 1. Create 2 Destroyers
    final Destroyer destroyer1 = new Destroyer("destroyer1");
    destroyer1.setSpeed(10);
    destroyer1.setNumberMissiles(30);
    destroyer1.setLength(300);
    final Destroyer destroyer2 = new Destroyer("destroyer2");
    destroyer2.setSpeed("20");
    destroyer2.setNumberMissiles("40");
    destroyer2.setLength(-350); // set to negative number to demo error checking

    // 2. Create 2 Submarines
    final Submarine submarine1 = new Submarine("submarine1");
    submarine1.setNumberTorpedos("Foo"); // set to non integer to demo error checking
    submarine1.setLength(200);
    submarine1.setSpeed(10);
    final Submarine submarine2 = new Submarine("submarine2");
    submarine2.setNumberTorpedos("6");
    submarine2.setLength(-220); // set to negative number to demo error checking
    submarine2.setSpeed(12);

    // 3. Create 2 P3s
    final P3 p3Aircraft1 = new P3("p3Aircraft1");
    p3Aircraft1.setAltitude(10_000);
    p3Aircraft1.setLength(100);
    p3Aircraft1.setNumberEngines(-2); // set to negative number to demo error checking
    p3Aircraft1.setSpeed(400);
    final P3 p3Aircraft2 = new P3("p3Aircraft2");
    p3Aircraft2.setAltitude(20_000);
    p3Aircraft2.setLength(200);
    p3Aircraft2.setNumberEngines(4);
    p3Aircraft2.setSpeed(600);


    // 4. Make a collection of Destroyers
    final Collection<Destroyer> destroyers = new ArrayList<>();
    destroyers.add(destroyer1);
    destroyers.add(destroyer2);

    // 5. Make a collection of Submarines
    final Collection<Submarine> submarines = new ArrayList<>();
    submarines.add(submarine1);
    submarines.add(submarine2);

    // 6. Make a collection that holds all Ships
    final Collection<Ship> ships = new ArrayList<>();
    ships.addAll(destroyers);
    ships.addAll(submarines);

    // 7. Make a collection that holds all Contacts
    final Collection<Contact> contacts = new ArrayList<>();
    contacts.addAll(ships);
    contacts.add(p3Aircraft1);
    contacts.add(p3Aircraft2);

    // 8. Print out Contacts to System.out
    printOutAllItems(contacts);


  }

  private static void printOutAllItems(final Collection<Contact> items) {
    for (final Contact item : items) {
      System.out.println(item);
    }
  }

}
