package jhu.webdevel.proj4;

import java.util.logging.Level;

/**
 * Class that represents a Submarine
 *
 * @author Adam Miller (amill189)
 *
 */
public class Submarine extends Ship {
  private int numberTorpedos;

  public Submarine(final String name) {
    super(Submarine.class, name);
  }

  public int getNumberTorpedos() {
    return this.numberTorpedos;
  }

  public void setNumberTorpedos(final int numberTorpedos) {
    this.numberTorpedos = getValidNumberOfTorpedos(numberTorpedos);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder("Name: ").append(getName()).append("\tType: ")
        .append(getType()).append("\tNumberTorpedos: ").append(getNumberTorpedos())
        .append("\tLength: ").append(getLength()).append("\tSpeed: ").append(getSpeed());
    return builder.toString();
  }

  private int getValidNumberOfTorpedos(final int numberTorpedos) {
    if (numberTorpedos < 0) {
      LOGGER.log(Level.WARNING, () -> String.format(
          "For %s, unable to set number of torpedos to a negative number, %d. Setting number of torpedos to zero.",
          getName(), numberTorpedos));
      return 0;
    }
    return numberTorpedos;
  }

  void setNumberTorpedos(final String numberTorpedos) {
    try {
      this.numberTorpedos = getValidNumberOfTorpedos(Integer.parseInt(numberTorpedos));
    } catch (final NumberFormatException e) {
      LOGGER.log(Level.WARNING, String.format(
          "For %s, unable to convert number of torpedos %s to an integer. Setting number of torpedos to two.",
          getName(), numberTorpedos), numberTorpedos);
      this.numberTorpedos = 2;
    }
  }
}
