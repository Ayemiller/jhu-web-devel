package jhu.webdevel.proj4;

import java.util.logging.Level;

/**
 * Class that represents a P3 aircraft
 *
 * @author Adam Miller (amill189)
 *
 */
public class P3 extends Aircraft {

  int numberEngines;

  public P3(final String name) {
    super(P3.class, name);
  }

  public int getNumberEngines() {
    return this.numberEngines;
  }

  public void setNumberEngines(final int numberEngines) {
    // if a negative number is entered as a number of engines, set a reasonable default of 2
    if (numberEngines < 0) {
      LOGGER.log(Level.WARNING, () -> String.format(
          "For %s, unable to set number of engines to a negative number, %d. Setting number of engines to 2.",
          getName(), numberEngines));
      this.numberEngines = 2;
    } else {
      this.numberEngines = numberEngines;
    }
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder("Name: ").append(getName()).append("\tType: ")
        .append(getType()).append("\tNumberEngines: ").append(getNumberEngines())
        .append("\tLength: ").append(getLength()).append("\tSpeed: ").append(getSpeed())
        .append("\tAltitude: ").append(getAltitude());

    return builder.toString();
  }

}
