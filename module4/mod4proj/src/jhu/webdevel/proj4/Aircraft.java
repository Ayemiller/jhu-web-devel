package jhu.webdevel.proj4;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract Class that represents an Aircraft
 *
 * @author Adam Miller (amill189)
 *
 */
public abstract class Aircraft implements Contact {

  protected static final Logger LOGGER = Logger.getLogger(Aircraft.class.getName());

  private int altitude;
  private int length;
  private String name;
  private int speed;
  private String type;

  public Aircraft(final Class<? extends Contact> clazz, final String name) {
    setName(name);
    setType(clazz.getSimpleName());
  }

  public int getAltitude() {
    return this.altitude;
  }

  @Override
  public int getLength() {
    return this.length;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public int getSpeed() {
    return this.speed;
  }

  @Override
  public String getType() {
    return this.type;
  }

  public void setAltitude(final int altitude) {
    this.altitude = altitude;
  }


  @Override
  public void setLength(final int length) {
    // if a negative number is entered as a length, set a reasonable default of 50
    if (length < 0) {
      LOGGER.log(Level.WARNING,
          () -> String.format(
              "For %s, unable to set length to a negative number, %d. Setting length to 50.",
              getName(), length));
      this.length = 50;
    } else {
      this.length = length;
    }
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public void setSpeed(final int speed) {
    this.speed = speed;
  }

  @Override
  public void setSpeed(final String speed) {
    this.speed = Integer.parseInt(speed);
  }

  @Override
  public void setType(final String type) {
    this.type = type;
  }

}
