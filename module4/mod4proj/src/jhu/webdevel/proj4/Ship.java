package jhu.webdevel.proj4;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that represents a ship
 *
 * @author Adam Miller (amill189)
 *
 */

public abstract class Ship implements Contact {

  protected static final Logger LOGGER = Logger.getLogger(Ship.class.getName());

  private int length;
  private String name;
  private int speed;
  private String type;

  public Ship(final Class<? extends Contact> clazz, final String name) {
    setName(name);
    setType(clazz.getSimpleName());
  }

  @Override
  public int getLength() {
    return this.length;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public int getSpeed() {
    return this.speed;
  }

  @Override
  public String getType() {
    return this.type;
  }


  @Override
  public void setLength(final int length) {
    // if a negative number is entered as a length, set a reasonable default of 100
    if (length < 0) {
      LOGGER.log(Level.WARNING,
          () -> String.format(
              "For %s, unable to set length to a negative number, %d. Setting length to 100.",
              getName(), length));
      this.length = 100;
    } else {
      this.length = length;
    }
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public void setSpeed(final int speed) {
    this.speed = speed;
  }

  /**
   * Set the speed by parsing a String argument to an Integer. If the parsing fails, set the speed
   * to 0.
   *
   * @param speed
   */
  @Override
  public void setSpeed(final String speed) {
    try {
      this.speed = Integer.parseInt(speed);
    } catch (final NumberFormatException e) {
      LOGGER.log(Level.WARNING, "Unable to convert {} to an integer. Setting speed to zero.",
          speed);
      this.speed = 0;
    }
  }

  @Override
  public void setType(final String type) {
    this.type = type;
  }

}
