package jhu.webdevel.proj4;

/**
 * Contact interface
 *
 * @author Adam Miller (amill189)
 *
 */
public interface Contact {

  public int getLength();

  public String getName();

  public int getSpeed();

  public String getType();

  public void setLength(int length);

  public void setName(String name);

  public void setSpeed(int speed);

  public void setSpeed(String speed);

  public void setType(String type);

}
