package jhu.webdevel.proj4;

import java.util.logging.Level;

/**
 * Class that represents a Destroyer
 *
 * @author Adam Miller (amill189)
 *
 */
public class Destroyer extends Ship {

  private int numberMissiles;

  public Destroyer(final String name) {
    super(Destroyer.class, name);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder("Name: ").append(getName()).append("\tType: ")
        .append(getType()).append("\tNumberMissiles: ").append(getNumberMissiles())
        .append("\tLength: ").append(getLength()).append("\tSpeed: ").append(getSpeed());
    return builder.toString();
  }

  private int getValidNumberOfMissiles(final int numberMissiles) {
    // if a negative number is entered as number of missiles, set a reasonable default of 0
    if (numberMissiles < 0) {
      LOGGER.log(Level.WARNING, () -> String.format(
          "For %s, unable to set number of missiles to a negative number, %d. Setting number of missiles to zero.",
          getName(), numberMissiles));
      return 0;
    }
    return numberMissiles;
  }

  int getNumberMissiles() {
    return this.numberMissiles;
  }

  void setNumberMissiles(final int numberMissiles) {
    this.numberMissiles = getValidNumberOfMissiles(numberMissiles);
  }

  /**
   * Set the number of missiles by parsing a String argument to an Integer. If the parsing fails,
   * set the number of missiles to 2.
   *
   * @param numberMissiles
   */
  void setNumberMissiles(final String numberMissiles) {
    try {
      this.numberMissiles = getValidNumberOfMissiles(Integer.parseInt(numberMissiles));
    } catch (final NumberFormatException e) {
      LOGGER.log(Level.WARNING, () -> String.format(
          "For %s, unable to convert number of missiles %s to an integer. Setting number of missiles to two.",
          getName(), numberMissiles));
      this.numberMissiles = 2;
    }
  }

}
