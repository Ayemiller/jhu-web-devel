package com.millera.model.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that connects to the MySQL database and executes the prepared statement and returns the
 * results.
 *
 * @author Adam
 *
 */
public class ReservationQuery {

  public static final String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
  private static final String DATE_FORMAT = "yyyy-MM-dd";
  private static final int GUIDE_FIRST_NAME_COL = 6;
  private static final int GUIDE_LAST_NAME_COL = 7;
  private static final Logger LOGGER = Logger.getLogger(ReservationQuery.class.getName());
  private static final String MYSQL_PASSWORD = "LetMeIn!";
  private static final String MYSQL_URL = "jdbc:mysql://web7.jhuep.com:3306/class?useSSL=false";
  private static final String MYSQL_USERNAME = "johncolter";
  private static final int RES_FIRST_NAME_COL = 1;
  private static final int RES_LAST_NAME_COL = 2;
  private static final int RES_LOCATION_COL = 5;
  private static final int RES_NUM_OF_DAYS_COL = 4;
  private static final int RES_START_DATE_COL = 3;

  public List<Reservation> executeQuery(final String dateString) throws ClassNotFoundException {
    final String query =
        "SELECT reservation.First, reservation.Last, reservation.StartDay, reservation.NumberOfDays, locations.location, guides.First, guides.Last FROM reservation, guides, locations WHERE reservation.StartDay >= ? ORDER BY reservation.StartDay";
    try {
      Class.forName(MYSQL_DRIVER);
    } catch (final ClassNotFoundException cnfe) {
      cnfe.printStackTrace();
    }
    /* Connect to MySQL and create a prepared statement. */
    try (
        Connection connection =
            DriverManager.getConnection(MYSQL_URL, MYSQL_USERNAME, MYSQL_PASSWORD);
        PreparedStatement preparedStatement = connection.prepareStatement(query)) {

      /* Convert the "String" date into the correct date type for the WHERE clause in our query. */
      final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
      final Date date = dateFormat.parse(dateString);
      preparedStatement.setDate(1, new java.sql.Date(date.getTime()));

      /* Set Logger format [YYYY-MM-DD HH:MM:SS] [<Log Level>] Message */
      System.setProperty("java.util.logging.SimpleFormatter.format",
          "[%1$tF %1$tT] [%4$s] %5$s %n");

      /* Execute the query, and create a list of our results. */
      LOGGER.log(Level.INFO, "Executing query: {0}", query);
      try (final ResultSet resultSet = preparedStatement.executeQuery()) {
        final List<Reservation> reservations = new ArrayList<>();

        /* Iterate through all of the rows in the ResultSet. */
        while (resultSet.next()) {

          /* Assign all of the columns in each result to our Reservation object */
          final Reservation reservation = new Reservation();
          reservation.setReservationFirstName(resultSet.getString(RES_FIRST_NAME_COL));
          reservation.setReservationLastName(resultSet.getString(RES_LAST_NAME_COL));
          reservation.setReservationStartDate(resultSet.getString(RES_START_DATE_COL));
          reservation.setReservationNumberOfDays(resultSet.getString(RES_NUM_OF_DAYS_COL));
          reservation.setLocation(resultSet.getString(RES_LOCATION_COL));
          reservation.setGuideFirstName(resultSet.getString(GUIDE_FIRST_NAME_COL));
          reservation.setGuideLastName(resultSet.getString(GUIDE_LAST_NAME_COL));
          reservations.add(reservation);
        }
        /* Return the list of reservations */
        return reservations;
      }
    } catch (final SQLException e) {
      LOGGER.log(Level.SEVERE, "Error handled when attempting to execute query. {0}",
          e.getMessage());
    } catch (final ParseException e) {
      LOGGER.log(Level.SEVERE, "Error handled when attempting to parse supplied date argument. {0}",
          e.getMessage());
    }

    /* If we are returning here, an error has occurred, so return an empty list. */
    return Collections.emptyList();
  }

}
