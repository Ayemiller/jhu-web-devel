package com.millera.model.beans;

import java.time.LocalDate;

/**
 * Class that serves as the data object for our JDBC query to get reservation info
 *
 * @author Adam
 *
 */
public class Reservation {


  /** The guide first name. */
  private String guideFirstName;

  /** The guide last name. */
  private String guideLastName;

  /** The location. */
  private String location;

  /** The reservation first name. */
  private String reservationFirstName;

  /** The reservation last name. */
  private String reservationLastName;

  /** The reservation number of days. */
  private String reservationNumberOfDays;

  /** The reservation start date. */
  private String reservationStartDate;

  /**
   * Instantiates a new reservation.
   */
  public Reservation() {
    super();
  }

  /**
   * Gets the guide's first name.
   *
   * @return the guide's first name
   */
  public String getGuideFirstName() {
    return this.guideFirstName;
  }

  /**
   * Gets the guide's last name.
   *
   * @return the guide's last name
   */
  public String getGuideLastName() {
    return this.guideLastName;
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public String getLocation() {
    return this.location;
  }

  /**
   * Gets the reservation's end date, based on adding the number of days to the starting date.
   *
   * @return the reservation end date
   */
  public String getReservationEndDate() {
    final LocalDate date = LocalDate.parse(this.reservationStartDate);
    return date.plusDays(Integer.parseInt(getReservationNumberOfDays()) - 1).toString();
  }

  /**
   * Gets the reservation's first name.
   *
   * @return the reservation first name
   */
  public String getReservationFirstName() {
    return this.reservationFirstName;
  }

  /**
   * Gets the reservation's last name.
   *
   * @return the reservation last name
   */
  public String getReservationLastName() {
    return this.reservationLastName;
  }

  /**
   * Gets the reservation's number of days.
   *
   * @return the reservation's number of days
   */
  public String getReservationNumberOfDays() {
    return this.reservationNumberOfDays;
  }

  /**
   * Gets the reservation's start date.
   *
   * @return the reservation's start date
   */
  public String getReservationStartDate() {
    return this.reservationStartDate;
  }

  /**
   * Sets the guide's first name.
   *
   * @param firstName guide's first name
   */
  public void setGuideFirstName(final String firstName) {
    this.guideFirstName = firstName;
  }

  /**
   * Sets the guide's last name.
   *
   * @param lastName the guide's last name
   */
  public void setGuideLastName(final String lastName) {
    this.guideLastName = lastName;
  }

  /**
   * Sets the location.
   *
   * @param location the location of the trip
   */
  public void setLocation(final String location) {
    this.location = location;
  }

  /**
   * Sets the reservation's first name.
   *
   * @param firstName the reservation's first name
   */
  public void setReservationFirstName(final String firstName) {
    this.reservationFirstName = firstName;
  }

  /**
   * Sets the reservation's last name.
   *
   * @param lastName the reservation's last name
   */
  public void setReservationLastName(final String lastName) {
    this.reservationLastName = lastName;
  }

  /**
   * Sets the reservation's number of days.
   *
   * @param numberOfDays the reservation's number of days
   */
  public void setReservationNumberOfDays(final String numberOfDays) {
    this.reservationNumberOfDays = numberOfDays;
  }

  /**
   * Sets the reservation's start date.
   *
   * @param startDate the reservation's start date
   */
  public void setReservationStartDate(final String startDate) {
    this.reservationStartDate = startDate;
  }

}
