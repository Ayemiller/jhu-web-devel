package com.millera.model.beans;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Java Bean class that passes info from our date selection page to our MySQL query.
 *
 * @author Adam
 *
 */
public class StartDate implements Serializable {


  private static final Logger LOGGER = Logger.getLogger(StartDate.class.getName());
  private static final long serialVersionUID = 938637226420533150L;
  private String reservationStartDate;

  /* Default constructor */
  public StartDate() {
    super();
  }

  /**
   * Get the minimum reservation start date.
   *
   * @return the minimum reservation start date
   */
  public String getReservationStartDate() {
    return this.reservationStartDate;
  }

  /**
   * Set the minimum reservation start date.
   *
   * @param startDate
   */
  public void setReservationStartDate(final String startDate) {
    /* Set Logger format [YYYY-MM-DD HH:MM:SS] [<Log Level>] Message */
    System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");

    LOGGER.log(Level.INFO, "Supplied reservation start date: {0}", startDate);
    this.reservationStartDate = startDate;
  }

}
