  <jsp:useBean id="hikeInfo" class="com.millera.model.beans.HikeInfo" scope="session" />
  <%@ page import="com.millera.model.Rates" %>
  <%@ page import="com.millera.model.beans.HikeInfo" %>
  <h2>Beartooth Hiking Trip Calculator</h2>
  <h6>(The hiking season is from June 1 to October 1)</h6>
        <% if (hikeInfo.getMessage() != null) { %>
        	<span style="color: red"><%=hikeInfo.getMessage() %></span><br />
        <% } %>
        <form action="Controller" method=POST>
        
        <table>
			<tr>
				<td>
					Starting Month:
				</td>
				<td>
					<select name="month">
	            	<% for(int month = HikeInfo.MIN_MONTH; month <= HikeInfo.MAX_MONTH; month++) { %>
	            		<option value="<%=month %>"><%=Integer.toString(month) %></option>
	            	<% } %>
	            	</select>
				</td>
			</tr>
			<tr>
				<td>
					Starting Day:
				</td>
				<td>
					<select name="day">
		           	<% for(int day = 1; day <= HikeInfo.MAX_DAYS_PER_MONTH; day++) { %>
		           		<option value="<%=day %>"><%=Integer.toString(day) %></option>
		           	<% } %>
		           	</select>
				</td>
			</tr>
			<tr>
				<td>
					Starting Year:
				</td>
				<td>
					<select name="year">
		           	<% for(int year = HikeInfo.MIN_YEAR; year <= HikeInfo.MAX_YEAR; year++) { %>
		           		<option value="<%=year %>"><%=Integer.toString(year) %></option>
		           	<% } %>
		           	</select>
				</td>
			</tr>
			<tr>
				<td>
					Possible Hikes:
				</td>
				<td>
					<select name="hike">
					<% for(Rates.HIKE hikeName : Rates.HIKE.values()) { %>
						<option value="<%=hikeName %>"><%=hikeName %></option>
					<% } %>
            		</select>
				</td>
			</tr>
			<tr>
				<td>
					Hike Duration:
				</td>
				<td>
					<select name="duration">
					<% for(int numDays : HikeInfo.VALID_DURATIONS) { %>
						<option value="<%=numDays %>"><%=numDays %></option>
					<% } %>
            		</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="SUBMIT">
				</td>
			</tr>
		</table>
		</form>
