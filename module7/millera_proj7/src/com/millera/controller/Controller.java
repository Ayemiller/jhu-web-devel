package com.millera.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.millera.model.BookingDay;
import com.millera.model.Rates;
import com.millera.model.beans.HikeInfo;


/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {

  public static final String DAY = "day"; // start day from form
  public static final String DURATION = "duration"; // hike duration from form
  public static final String HIKE = "hike"; // chosen hike from form
  public static final String HIKE_INFO = "hikeInfo"; // model bean instance
  public static final String MONTH = "month"; // start month from form
  public static final String YEAR = "year"; // start year from form
  private static final long serialVersionUID = -5303135997157690498L;
  private static final String USE_CONTROLS_ERROR_MSG =
      "Please provide input by using the drop-down controls below!";


  /**
   * @see HttpServlet#HttpServlet()
   */
  public Controller() {
    super();
  }

  /**
   * Provide more informative error messages to inform the user what to do.
   *
   * @param rateForHike
   * @param validDuration
   * @return
   */
  private String processErrorMessage(final String message, final boolean validDuration) {
    if ("begin or end date was out of season".equals(message)) {
      return "Please select a beginning date and duration for your hike so that the entirety\n"
          + " of your hike starts on or after June 1 and ends on or before October 1.";
    } else if ("One of the dates was not a valid day".equals(message)) {
      return "The chosen starting date for your hike is not a valid calendar day.";
    } else if ("One of the dates was not defined".equals(message) && !validDuration) {
      return "Please select a valid duration for the chosen hike.";
    }
    return message;
  }

  /**
   * Manually check to make sure user input is within valid boundaries.
   *
   * @param month (from input)
   * @param day (from input)
   * @param year (from input)
   * @param hike (from input)
   * @param duration (from input)
   */
  private void validateInput(final String month, final String day, final String year,
      final String hike, final String duration) {
    final List<String> hikeList =
        Stream.of(Rates.HIKE.values()).map(Enum::name).collect(Collectors.toList());
    final List<Integer> durationList =
        Arrays.stream(HikeInfo.VALID_DURATIONS).boxed().collect(Collectors.toList());
    if (!hikeList.contains(hike)) {
      throw new IllegalArgumentException("Invalid hike");
    } else if (Integer.parseInt(month) < HikeInfo.MIN_MONTH
        || Integer.parseInt(month) > HikeInfo.MAX_MONTH) {
      throw new IllegalArgumentException("Invalid month");
    } else if (Integer.parseInt(day) < 1 || Integer.parseInt(day) > HikeInfo.MAX_DAYS_PER_MONTH) {
      throw new IllegalArgumentException("Invalid day");
    } else if (Integer.parseInt(year) < HikeInfo.MIN_YEAR
        || Integer.parseInt(year) > HikeInfo.MAX_YEAR) {
      throw new IllegalArgumentException("Invalid year");
    } else if (!durationList.contains(Integer.parseInt(duration))) {
      throw new IllegalArgumentException("Invalid duration");
    }
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {
    final HttpSession session = request.getSession();
    final ServletContext servletContext = getServletContext();
    HikeInfo hikeInfo = (HikeInfo) session.getAttribute(HIKE_INFO);
    response.setContentType("text/html;charset=UTF-8");
    // Create our HikeInfo bean if null
    if (hikeInfo == null) {
      hikeInfo = new HikeInfo();
      session.setAttribute(HIKE_INFO, hikeInfo);
      final RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/index.jsp");
      dispatcher.forward(request, response);
    } else {

      // Set variables from the request that was made
      final String month = request.getParameter(MONTH);
      final String day = request.getParameter(DAY);
      final String year = request.getParameter(YEAR);
      final String hike = request.getParameter(HIKE);
      final String duration = request.getParameter(DURATION);

      if (month == null || day == null || year == null || hike == null || duration == null) {
        // invalid input detected, likely through URL attempt
        hikeInfo.setMessage(USE_CONTROLS_ERROR_MSG);
        final RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // Set the properties of our "model bean" from the values in the request.
      hikeInfo.setMonth(month);
      hikeInfo.setDay(day);
      hikeInfo.setYear(year);
      hikeInfo.setHike(hike);
      hikeInfo.setDuration(duration);
      final Rates rateForHike;
      final BookingDay beginDate;
      // Figure out the cost of the hike and validate the input
      try {

        rateForHike = new Rates(Rates.HIKE.valueOf(hike));
        beginDate =
            new BookingDay(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day));
        validateInput(month, day, year, hike, duration);
      } catch (final IllegalArgumentException e) {
        // provided input was not able to be handled, print error message telling user to use the
        // provided controls to enter input
        hikeInfo.setMessage(USE_CONTROLS_ERROR_MSG);
        final RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
        return;
      }
      rateForHike.setBeginDate(beginDate);
      final boolean validDuration = rateForHike.setDuration(Integer.parseInt(duration));

      // Store the calculated results if our duration input was valid
      if (validDuration) {
        hikeInfo.setCost(rateForHike.getCost());
        hikeInfo.setBeginDate(rateForHike.getBeginBookingDay());
        hikeInfo.setEndDate(rateForHike.getEndBookingDay());
        hikeInfo.setNormalDays(rateForHike.getNormalDays());
        hikeInfo.setPremiumDays(rateForHike.getPremiumDays());
        hikeInfo.setBaseRate(rateForHike.getBaseRate());
        hikeInfo.setPremiumRate(rateForHike.getPremiumRate());
      }

      // Display the cost page if all input was valid
      if (rateForHike.isValidDates()) {
        final RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/cost.jsp");
        dispatcher.forward(request, response);
      } else {
        // Cannot calculate estimate, explain to user why
        hikeInfo.setMessage(processErrorMessage(rateForHike.getDetails(), validDuration));
        final RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
      }
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }

}
