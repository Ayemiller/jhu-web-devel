package com.millera.model.beans;

import com.millera.model.BookingDay;

/**
 * Java Bean class that serves as the model for our servlet. Responsible for passing information to
 * and from the view.
 *
 * @author Adam
 *
 */
public class HikeInfo {

  private static final String DOLLAR_FORMAT = "$%.2f";
  public static final int MAX_DAYS_PER_MONTH = 31;
  public static final int MAX_MONTH = 9;
  public static final int MAX_YEAR = 2040;
  public static final int MIN_MONTH = 6;
  public static final int MIN_YEAR = 2007;
  public static final int[] VALID_DURATIONS = {2, 3, 4, 5, 7};

  private String baseRate = ""; // cost per "normal" day
  private String beginDate = ""; // starting date
  private Double cost = -0.01d; // total cost
  private String costString = ""; // total cost string
  private String day = ""; // starting day from form
  private String duration = ""; // duration from form
  private String endDate = ""; // calculated end date
  private String hike = ""; // chosen hike from form
  private String message = ""; // message from Rates class
  private String month = ""; // month from form
  private String normalDays = ""; // number of "normal" days
  private String premiumDays = ""; // number of premium days
  private String premiumRate = ""; // cost per premium day

  private String year = "";

  public HikeInfo() {
    super();
  }

  /**
   * Gets the base rate.
   *
   * @return the base rate
   */
  public String getBaseRate() {
    return this.baseRate;
  }

  /**
   * Gets the begin date.
   *
   * @return the begin date
   */
  public String getBeginDate() {
    return this.beginDate;
  }

  /**
   * Gets the cost.
   *
   * @return the cost
   */
  public Double getCost() {
    return this.cost;
  }

  /**
   * Gets the cost string.
   *
   * @return the cost string
   */
  public String getCostString() {
    return this.costString;
  }

  /**
   * Gets the day.
   *
   * @return the day
   */
  public String getDay() {
    return this.day;
  }

  /**
   * Gets the duration.
   *
   * @return the duration
   */
  public String getDuration() {
    return this.duration;
  }

  /**
   * Gets the end date.
   *
   * @return the end date
   */
  public String getEndDate() {
    return this.endDate;
  }

  /**
   * Gets the hike.
   *
   * @return the hike
   */
  public String getHike() {
    return this.hike;
  }

  /**
   * Gets the message.
   *
   * @return the message
   */
  public String getMessage() {
    return this.message;
  }

  /**
   * Gets the month.
   *
   * @return the month
   */
  public String getMonth() {
    return this.month;
  }

  /**
   * Gets the normal days.
   *
   * @return the normal days
   */
  public String getNormalDays() {
    return this.normalDays;
  }

  /**
   * Gets the premium days.
   *
   * @return the premium days
   */
  public String getPremiumDays() {
    return this.premiumDays;
  }

  /**
   * Gets the premium rate.
   *
   * @return the premium rate
   */
  public String getPremiumRate() {
    return this.premiumRate;
  }

  /**
   * Gets the year.
   *
   * @return the year
   */
  public String getYear() {
    return this.year;
  }

  /**
   * Sets the base rate.
   *
   * @param baseRate the new base rate
   */
  public void setBaseRate(final double baseRate) {
    this.baseRate = String.format(DOLLAR_FORMAT, baseRate);
  }

  /**
   * Sets the begin date.
   *
   * @param bookingDay the new begin date
   */
  public void setBeginDate(final BookingDay bookingDay) {
    this.beginDate = bookingDay.toString();
  }

  /**
   * Sets the cost.
   *
   * @param cost the new cost
   */
  public void setCost(final Double cost) {
    this.cost = cost;
    setCostString(String.format(DOLLAR_FORMAT, cost));
  }

  /**
   * Sets the cost string.
   *
   * @param costString the new cost string
   */
  public void setCostString(final String costString) {
    this.costString = costString;
  }

  /**
   * Sets the day.
   *
   * @param day the new day
   */
  public void setDay(final String day) {
    this.day = day;
  }

  /**
   * Sets the duration.
   *
   * @param duration the new duration
   */
  public void setDuration(final String duration) {
    this.duration = duration;
  }

  /**
   * Sets the end date.
   *
   * @param endDay the new end date
   */
  public void setEndDate(final BookingDay endDay) {
    this.endDate = endDay.toString();
  }

  /**
   * Sets the hike.
   *
   * @param hike the new hike
   */
  public void setHike(final String hike) {
    this.hike = hike;
  }

  /**
   * Sets the message.
   *
   * @param details the new message
   */
  public void setMessage(final String details) {
    this.message = details;
  }

  /**
   * Sets the month.
   *
   * @param month the new month
   */
  public void setMonth(final String month) {
    this.month = month;
  }

  /**
   * Sets the normal days.
   *
   * @param normalDays the new normal days
   */
  public void setNormalDays(final int normalDays) {
    this.normalDays = Integer.toString(normalDays);
  }

  /**
   * Sets the premium days.
   *
   * @param premiumDays the new premium days
   */
  public void setPremiumDays(final int premiumDays) {
    this.premiumDays = Integer.toString(premiumDays);
  }

  /**
   * Sets the premium rate.
   *
   * @param premiumRate the new premium rate
   */
  public void setPremiumRate(final double premiumRate) {
    this.premiumRate = String.format(DOLLAR_FORMAT, premiumRate);
  }

  /**
   * Sets the year.
   *
   * @param year the new year
   */
  public void setYear(final String year) {
    this.year = year;
  }

}
