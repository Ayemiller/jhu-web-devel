package com.millera.model;



import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Rates is an object that provides a cost for a booked tour. There is a base rate and a premium
 * rate that are used to calculate the cost of the tour. Weekdays use the base rate, weekends use
 * the premium rate.
 *
 * The premium rate is automatically generated as 1.5x the base rate
 *
 * @author evansrb1
 * @version 1.1 Removed setEndDate() method
 */
public class Rates {
  public static enum HIKE {
    BEATEN, GARDINER, HELLROARING
  }

  /** Quick test of the class */
  public static void main(final String[] argv) {
    final BookingDay startDay = new BookingDay(2008, 7, 1);
    final BookingDay endDay = new BookingDay(2008, 7, 7);
    System.out.println(
        "start Day of " + startDay + " " + (startDay.isValidDate() ? "is valid" : "is not valid"));
    System.out
        .println("end Day " + endDay + " " + (endDay.isValidDate() ? "is valid" : "is not valid"));

    final Rates rates = new Rates(HIKE.BEATEN);

    rates.setBeginDate(startDay);
    final boolean success = rates.setDuration(7);
    System.out.println("duration was " + (success ? "good" : "bad"));
    System.out.println("valid Dates = " + rates.isValidDates());
    if (rates.isValidDates()) {
      System.out.println("Cost of trip = " + rates.getCost());
      System.out.println("Weekdays: " + rates.getNormalDays());
      System.out.println("Weekends: " + rates.getPremiumDays());
    } else {
      System.out.println("Sorry, but " + rates.getDetails());
    }
  }

  // weekday rate
  private int baseRate = -1;
  // start of trip
  private BookingDay beginDate = null;
  private String details = "none";
  // end of trip
  private BookingDay endDate = null;
  private int premiumRate = -1;

  // cached calculated rate value
  private int rate = 0;
  private int seasonEndDay = 1;

  // we close Oct 1st.
  private int seasonEndMonth = 10;
  private int seasonStartDay = 1;

  // we open June 1st
  private int seasonStartMonth = 6;
  // flag to indicate total rate and day types need to be recalculated
  private boolean synched = false;

  private int[] validDurations = null;
  // cached number of weekday days
  int normalDays = 0;

  // cached number of weekend days
  int premiumDays = 0;

  public Rates(final HIKE hike) {

    switch (hike) {
      case GARDINER:
        this.baseRate = 4000;
        this.validDurations = new int[2];
        this.validDurations[0] = 3;
        this.validDurations[1] = 5;
        break;
      case HELLROARING:
        this.baseRate = 3500;
        this.validDurations = new int[3];
        this.validDurations[0] = 2;
        this.validDurations[1] = 3;
        this.validDurations[2] = 4;
        break;
      case BEATEN:
        this.baseRate = 4500;
        this.validDurations = new int[2];
        this.validDurations[0] = 5;
        this.validDurations[1] = 7;
        break;
    }
    this.premiumRate = this.baseRate + this.baseRate / 2;
  }


  /**
   * Get the base rate for a weekday
   * 
   * @return the weekend rate
   */
  public double getBaseRate() {
    return this.baseRate / 100.0;
  }

  /**
   * Get the beginning date of the reservation
   *
   * @return a BookingDay object of the beginning date
   */
  public BookingDay getBeginBookingDay() {
    return this.beginDate;
  }


  /**
   * Get the beginning date of the reservation
   *
   * @return a GregorianCalendar object of the beginning date
   */
  public GregorianCalendar getBeginDate() {
    if (this.beginDate == null) {
      return null;
    } else {
      return this.beginDate.getDate();
    }
  }

  /**
   * Get the total cost for the trip. Returns -0.01 is something is amiss.
   * 
   * @return the cost of the trip
   */
  public double getCost() {
    if (this.synched) {
      return this.rate / 100.0;
    } else {
      return calculateCost() / 100.0;
    }
  }

  /**
   * Status of validation of data. This is filled in once isValidDates() is called
   * 
   * @return A String indicating the status of the input data validity
   */
  public String getDetails() {
    return this.details;
  }

  // Get the valid durations for this hike.
  public int[] getDurations() {
    return this.validDurations;
  }

  /**
   * Get the end date of the reservation
   *
   * @return a GregorianCalendar object of the end date
   */
  public BookingDay getEndBookingDay() {
    return this.endDate;
  }

  /**
   * Get the end date of the reservation
   *
   * @return a GregorianCalendar object of the end date
   */
  public GregorianCalendar getEndDate() {
    if (this.endDate == null) {
      return null;
    } else {
      return this.endDate.getDate();
    }
  }

  /**
   * Get the number of weekdays in the reservation
   * 
   * @return number of weekdays in the reservation
   */
  public int getNormalDays() {
    if (!this.synched) {
      calculateCost();
    }
    return this.normalDays;
  }

  /**
   * Get the number of weekend days in the reservation
   * 
   * @return number of weekend days in the reservation
   */
  public int getPremiumDays() {
    if (!this.synched) {
      calculateCost();
    }
    return this.premiumDays;
  }

  /**
   * Get the base rate for a weekend
   * 
   * @return the weekend rate
   */
  public double getPremiumRate() {
    return this.premiumRate / 100.0;
  }

  /** Get the end day of the season */
  public int getSeasonEndDay() {
    return this.seasonEndDay;
  }

  /** Get the end month of the season */
  public int getSeasonEndMonth() {
    return this.seasonEndMonth;
  }

  /** Get the start day of the season */
  public int getSeasonStartDay() {
    return this.seasonStartDay;
  }

  /** Get the start month of the season */
  public int getSeasonStartMonth() {
    return this.seasonStartMonth;
  }

  /**
   * Determine if the entered dates are valid
   * 
   * @return true if both days exist and the begin date is before the end date
   */
  public boolean isValidDates() {
    if (this.beginDate == null || this.endDate == null) {
      this.details = "One of the dates was not defined";
      return false;
    } else if (this.beginDate.getYear() != this.endDate.getYear()) {
      this.details = "The begin and end date must be within the same year";
      return false;
    } else if (this.beginDate.equals(this.endDate)) {
      this.details = "The begin and end date must not be the same date";
      return false;
    } else if (this.beginDate.isValidDate() && this.endDate.isValidDate()) {
      if (!this.beginDate.after(this.endDate)) {
        if (!this.beginDate.before(this.seasonStartMonth, this.seasonStartDay)
            && !this.endDate.after(this.seasonEndMonth, this.seasonEndDay)) {
          this.details = "valid dates";
          return true;
        } else {
          this.details = "begin or end date was out of season";
          return false;
        }
      } else {
        this.details = "end date was before begin date";
        return false;
      }
    } else {
      this.details = "One of the dates was not a valid day";
      return false;
    }

  }

  /**
   * Set the beginning date of the reservation
   *
   * @param beginDate the beginning date
   */
  public void setBeginDate(final BookingDay beginDate) {
    this.beginDate = beginDate;
    this.synched = false;
  }

  /**
   * Set the duration of the reservation. One day hikes means no overnight, a two day hike is two
   * days, one night.
   * 
   * Note that if this method is used with a start date, it will update the end date to match the
   * duration given.
   *
   * @param int the duration of the hike
   */
  public boolean setDuration(final int days) {
    boolean valid = false;
    for (final int d : this.validDurations) {
      if (days == d) {
        valid = true;
        break;
      }
    }
    if (!valid) {
      return false;
    } else {
      // first a quick check to see if this is a valid
      final GregorianCalendar day = this.beginDate.getDate();
      day.add(Calendar.DAY_OF_MONTH, days - 1);
      this.endDate = new BookingDay(day.get(Calendar.YEAR), day.get(Calendar.MONTH) + 1,
          day.get(Calendar.DAY_OF_MONTH));
      this.synched = false;
      return true;
    }
  }

  /**
   * Set the ending day for the season. This is used to validate booking dates. Default is October
   * 1st.
   *
   * @param month (1-Jan, 12-Dec)
   * @param day
   */
  public void setSeasonEnd(final int month, final int day) {
    this.seasonEndMonth = month;
    this.seasonEndDay = day;
    this.synched = false;
  }

  /**
   * Set the starting day for the season. This is used to validate booking dates. Default is June
   * 1st.
   *
   * @param month (1-Jan, 12-Dec)
   * @param day
   */
  public void setSeasonStart(final int month, final int day) {
    this.seasonStartMonth = month;
    this.seasonStartDay = day;
    this.synched = false;
  }

  // calculate the cost, returns -1 if dates are bad
  // bug fix courtesy of Zachary Schmook! 8/7/2010
  private int calculateCost() {
    if (!isValidDates()) {
      return -1;
    }
    this.premiumDays = 0;
    this.normalDays = 0;

    // Okay, if we got to here, the dates are somewhat sane...
    final GregorianCalendar day = this.beginDate.getDate();
    final GregorianCalendar endDay = this.endDate.getDate();
    while (day.before(endDay)) {
      final int dayOfWeek = day.get(Calendar.DAY_OF_WEEK);
      if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
        this.premiumDays++;
      } else {
        this.normalDays++;
      }
      day.add(Calendar.DAY_OF_WEEK, 1);
    }
    if (endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
        || endDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
      this.premiumDays++;
    } else {
      this.normalDays++;
    }
    // System.out.println("normal days = " + normalDays + ", premium days = " + premiumDays);
    this.rate = this.normalDays * this.baseRate + this.premiumDays * this.premiumRate;
    this.synched = true;
    // System.out.println("baseRate = " + baseRate + ", premiumRate = " + premiumRate + ", rate = "
    // + rate);
    return this.rate;
  }
}

