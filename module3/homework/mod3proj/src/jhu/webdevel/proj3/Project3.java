package jhu.webdevel.proj3;

/**
 * Module 3 Java Project to multiply two command-line arguments.
 *
 * @author Adam Miller (amill189)
 *
 */
public class Project3 {

  private static final int REQUIRED_NUMBER_OF_ARGS = 2;

  /**
   * Main method that attempts to multiple two provided arguments as integers. If an incorrect
   * number of arguments is provided, or if the conversion of an argument from String to int fails,
   * error messages will be printed.
   *
   * @param args
   */
  public static void main(final String[] args) {

    // check to make sure two arguments are provided
    if (args.length != REQUIRED_NUMBER_OF_ARGS) {
      System.err.println("Please provide two integer command-line arguments.");
    } else {
      try {
        // convert the String arguments to int
        final int operand1 = Integer.parseInt(args[0]);
        final int operand2 = Integer.parseInt(args[1]);

        // multiply the arguments
        final int product = multiply(operand1, operand2);

        // output the product of the multiplication as described
        final StringBuilder builder = new StringBuilder();
        if (product < 0) {
          builder.append("(").append(Math.abs(product)).append(")");
        } else {
          builder.append(product);
        }
        System.out.println(builder.toString());

      } catch (final NumberFormatException e) {
        // the provided arguments were not able to be converted to integers, print an error
        System.err.println("Unable to parse arguments to integer");
      }
    }
  }

  /**
   * Method to multiply two integers
   *
   * @param value1 first operand
   * @param value2 second operand
   * @return the product of the two operands
   */
  private static int multiply(final int value1, final int value2) {
    return value1 * value2;
  }

}
