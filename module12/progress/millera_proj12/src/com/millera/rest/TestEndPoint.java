package com.millera.rest;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("example")
public class TestEndPoint {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/params/{id}")
  public String getParamText(@PathParam("id") final String id, @QueryParam("zip") final int zip) {
    return "The value of the path parameter is " + id + " with a zip code " + zip;
  }

  @POST
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/querytest")
  public String getParamText(@FormParam("username") final String user,
      @FormParam("password") final String pw) {
    return "The value of the form parameters are username=" + user + " with a password =" + pw;
  }


  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String getText() {
    return "SUCCESSFUL OUTPUT FROM SERVICE";
  }

}
