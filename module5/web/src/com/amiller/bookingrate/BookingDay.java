/*
 * A simple object to keep represent a Day.  It has getter and setters for day,
 * month and year so that it can be used as a bean.
 *
 * getDate() returns a GregorianCalendar object that can be used for date
 * calcualtions
 *
 * isValidDate() is used to see if the day/month/year is a valid date
 * getValidationStatus() returns a string message for problems
 *
 * Internally, months are from 0-11 (like the GregorianCalendar) but externally
 * they run 1-12
 */

package com.amiller.bookingrate;

import java.util.GregorianCalendar;
// version 3, Last updated 6/28/2020.  Updated valid years to go out to 2040

/**
 * @author evansrb1
 */
public class BookingDay {

  private int year = 0;
  private int month = 0;
  private int dayOfMonth = 0;
  private String validation = BookingDay.NOT_VALIDATED;

  public final static String NOT_VALID = "Incorrect Date Format";
  public final static String NOT_VALIDATED = "NOT VALIDATED";
  public final static String VALID = "VALID";

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BookingDay other = (BookingDay) obj;
    if (this.year != other.year) {
      return false;
    }
    if (this.month != other.month) {
      return false;
    }
    if (this.dayOfMonth != other.dayOfMonth) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 53 * hash + this.year;
    hash = 53 * hash + this.month;
    hash = 53 * hash + this.dayOfMonth;
    return hash;
  }

  /**
   * Constructor to use for Bean purposes.  Must set the three fields manually
   */
  public BookingDay() {
  }

  /**
   * Constructor for testing purposes or use within the bookingrate package.
   *
   * @param year        the year, from 2007 to 2040
   * @param month,      the month Jan = 1, Dec = 12
   * @param dayOfMonth, the numerical day of the month
   */
  public BookingDay(final int year, final int month, final int dayOfMonth) {
    //    System.out.println("BookingDay(" + year +"," + month + "," + dayOfMonth + ")");
    this.year = year;
    this.month = month - 1;
    this.dayOfMonth = dayOfMonth;
  }


  /**
   * Do the date fields describe a valid date?
   *
   * @return true if the year is between 2007 and 2020, and the day and month are valid dates
   */
  public boolean isValidDate() {
    final StringBuilder builder = new StringBuilder();
    if ((this.year < 2007 || this.year > 2040) ||
        (this.month < 0 || this.month > 11) ||
        badDay()) {
      this.validation = BookingDay.NOT_VALID;
      return false;
    } else {
      this.validation = BookingDay.VALID;
      return true;
    }
  }


  /**
   * is this day before the endDay given
   *
   * @param endDay The end day
   * @return true if this day is before the end day
   */
  public boolean before(final BookingDay endDay) {
    if (endDay == null) {
      return false;
    }
    if (this.year < endDay.year) {
      return true;
    } else if (this.year > endDay.year) {
      return false;
    }
    if (this.month < endDay.month) {
      return true;
    } else if (this.month > endDay.month) {
      return false;
    }
    if (this.dayOfMonth < endDay.dayOfMonth) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * is this day after the endDay given
   *
   * @param endDay The end day
   * @return true if this day is after the end day
   */
  public boolean after(final BookingDay endDay) {
    if (endDay == null) {
      return false;
    }
    if (this.year > endDay.year) {
      return true;
    } else if (this.year < endDay.year) {
      return false;
    }
    if (this.month > endDay.month) {
      return true;
    } else if (this.month < endDay.month) {
      return false;
    }
    if (this.dayOfMonth > endDay.dayOfMonth) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Test to see if this BookingDay is after the month and day specified. Note that there is no
   * error checking on the month/day combo, also, this assumes the same year
   *
   * @param month the month (1-Jan, 12-Dec)
   * @param day   the day of the month
   * @return true if this Booking day is after the provided date
   */
  public boolean after(final int month, final int day) {
    if (this.month + 1 > month) {
      return true;
    } else if (this.month + 1 < month) {
      return false;
    } else {
      if (this.dayOfMonth > day) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Test to see if this BookingDay is before the month and day specified. Note that there is no
   * error checking on the month/day combo
   *
   * @param month the month (1-Jan, 12-Dec)
   * @param day   the day of the month
   * @return true if this Booking day is before the provided date
   */
  public boolean before(final int month, final int day) {
    if (this.month + 1 < month) {
      return true;
    } else if (this.month + 1 > month) {
      return false;
    } else {
      if (this.dayOfMonth < day) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Extra information of validation status of this date data
   *
   * @return a string explaining what is right or wrong
   */
  public String getValidationStatus() {
    return this.validation;
  }


  @Override
  public String toString() {
    return "" + (this.month + 1) + "/" + this.dayOfMonth + "/" + this.year;
  }

  /**
   * is the day value out of range for the given month?
   *
   * @return true if it is a bad day
   */
  protected boolean badDay() {
    switch (this.month) {
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:
      case 9:
      case 11:
        return (this.dayOfMonth < 1 || this.dayOfMonth > 31);
      case 8:
      case 3:
      case 5:
      case 10:
        return (this.dayOfMonth < 1 || this.dayOfMonth > 30);
      case 1:
        if ((this.year % 4) == 0) {
          return (this.dayOfMonth < 1 || this.dayOfMonth > 29);
        } else {
          return (this.dayOfMonth < 1 || this.dayOfMonth > 28);
        }
      default:
        return true;
    }
  }


  /**
   * Get a GregorianCalendar object that represents this date
   *
   * @return the GregorianCalendar object for this date
   */
  public GregorianCalendar getDate() {
//        System.out.println("getDate(" + year + "," + month + "," + dayOfMonth + "0");
    return new GregorianCalendar(this.year, this.month, this.dayOfMonth);
  }

  /**
   * Get the year of this date
   *
   * @return the year
   */
  int getYear() {
    return this.year;
  }

  /**
   * Set the year of this date.  This invalidates the BookingDay object, and isValidDate() must be
   * called again
   *
   * @param year
   */
  public void setYear(final int year) {
    this.validation = BookingDay.NOT_VALIDATED;
    this.year = year;
  }

  /**
   * Get the month of this date
   *
   * @return the month (Jan = 1, Dec = 12)
   */
  public int getMonth() {
    return this.month + 1;

  }

  /**
   * Set the month of this date
   *
   * @param month (Jan = 1, Dec = 12)
   */
  public void setMonth(final int month) {
    this.validation = BookingDay.NOT_VALIDATED;
    this.month = month - 1;
  }

  /**
   * Get the day of the month
   *
   * @return the day of the month
   */
  public int getDayOfMonth() {
    return this.dayOfMonth;
  }

  /**
   * Set the day of the month
   *
   * @param dayOfMonth
   */
  public void setDayOfMonth(final int dayOfMonth) {
    this.dayOfMonth = dayOfMonth;
  }

  public static void main(final String[] argv) {
    final BookingDay day = new BookingDay(2008, 6, 15);
    System.out.println("Day = " + day);
    System.out.println("Day is before 5/1? " + day.before(5, 1));
    System.out.println("Day is before 9/1? " + day.before(9, 1));

  }
}