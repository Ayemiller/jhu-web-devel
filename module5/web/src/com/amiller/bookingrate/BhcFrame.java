package com.amiller.bookingrate;

import com.amiller.bookingrate.Rates.HIKE;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.time.LocalDate;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Swing frame that calculates cost and scheduling information for BHC hikes.
 */
public class BhcFrame {

  private static final int NUM_MONTHS_IN_YEAR = 12;
  private static final int EARLIEST_YEAR = 2007;
  private static final int LATEST_YEAR = 2040;
  private static final int MAX_NUM_DAYS_IN_MONTH = 31;

  private JComboBox day;
  private JComboBox month;
  private JComboBox year;
  private JComboBox duration;
  private JTextField costTextField;
  private JButton calculateCost;
  private JComboBox hikeSelection;
  private JPanel mainPanel1;
  private JLabel monthLabel;
  private JLabel dayLabel;
  private JLabel yearLabel;
  private JLabel hikesLabel;
  private JLabel durationLabel;
  private JLabel costLabel;
  private JLabel endDateLabel;
  private JTextField endDateTextField;
  private JLabel costNotesLabel;
  private JTextField costNotesTextField;
  private JTextArea tripDetailsTextArea;
  private JLabel tripDetailsLabel;
  private JLabel bhcTitleLabel;
  private LocalDate today;

  public static void main(final String[] args) {
    final JFrame frame = new JFrame("BhcFrame");
    final BhcFrame bhcFrame = new BhcFrame();
    frame.setContentPane(bhcFrame.mainPanel1);
    bhcFrame.today = LocalDate.now();

    // add the months and select the current month
    bhcFrame.populateMonths();

    // add the years and select the current year
    bhcFrame.populateYears();

    // add the days and select the current day
    bhcFrame.populateDays();

    // add action listener to hike selection combobox to populate the valid durations
    bhcFrame.addHikeSelectionActionListener(bhcFrame);

    // add the three possible hikes and select the first
    bhcFrame.populateHikes();

    // add action listener for the calculate button
    bhcFrame.addCalculateActionListener(bhcFrame);

    // add action listener to clear the calculated info when a selection is modified
    bhcFrame.addClearingActionListeners(bhcFrame);

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();

    frame.setVisible(true);

  }

  /**
   * Populate the Hikes combobox
   */
  private void populateHikes() {
    for (final HIKE hikeName : HIKE.values()) {
      this.hikeSelection.addItem(hikeName);
    }
    this.hikeSelection.setSelectedItem(0);
  }

  /**
   * Populate the start day combobox and select today's day.
   */
  private void populateDays() {
    final int currentDay = LocalDate.now().getDayOfMonth();
    for (int day = 1; day <= BhcFrame.MAX_NUM_DAYS_IN_MONTH; day++) {
      final String dayString = Integer.toString(day);
      this.day.addItem(dayString);
      if (day == currentDay) {
        this.day.setSelectedItem(dayString);
      }
    }
  }

  /**
   * Populate the start month combobox and select the current month.
   */
  private void populateMonths() {
    for (int i = 1; i <= BhcFrame.NUM_MONTHS_IN_YEAR; i++) {
      this.month.addItem(Integer.toString(i));
    }
    this.month.setSelectedIndex(this.today.getMonthValue() - 1); // subtract 1 since Jan = 0
  }

  /**
   * Populate the start year combobox and select the current year.
   */
  private void populateYears() {
    // add the years from 2007 to 2040 and select the current year
    final int currentYear = LocalDate.now().getYear();
    for (int year = BhcFrame.EARLIEST_YEAR; year <= BhcFrame.LATEST_YEAR; year++) {
      final String yearString = Integer.toString(year);
      this.year.addItem(yearString);
      if (year == currentYear) {
        this.year.setSelectedItem(yearString);
      }
    }
  }

  /**
   * Adds action listener to populate the valid durations when a hike is selected.
   *
   * @param bhcFrame
   */
  private void addHikeSelectionActionListener(final BhcFrame bhcFrame) {
    bhcFrame.hikeSelection.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        bhcFrame.duration.removeAllItems();
        final HIKE selectedHike = (HIKE) bhcFrame.hikeSelection.getSelectedItem();
        final Rates rateForHike = new Rates(selectedHike);
        for (final int duration : rateForHike.getDurations()) {
          bhcFrame.duration.addItem(Integer.toString(duration));
        }
      }
    });
  }

  /**
   * Method to add the action listener for the calculate button.
   * <p>
   * Display information for valid input that includes total cost, end date and pricing breakdown.
   * <p>
   * If invalid input is encountered, show a JOptionPane to guide the user.
   *
   * @param bhcFrame
   */
  private void addCalculateActionListener(final BhcFrame bhcFrame) {
    this.calculateCost.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        final HIKE selectedHike = (HIKE) bhcFrame.hikeSelection.getSelectedItem();
        final Rates rateForHike = new Rates(selectedHike);

        // set the starting date
        final int startingYear = Integer.parseInt((String) bhcFrame.year.getSelectedItem());
        final int startingMonth = Integer.parseInt((String) bhcFrame.month.getSelectedItem());
        final int startingDay = Integer.parseInt((String) bhcFrame.day.getSelectedItem());

        final BookingDay beginDate = new BookingDay(startingYear, startingMonth, startingDay);

        rateForHike.setBeginDate(beginDate);
        rateForHike.setDuration(Integer.parseInt((String) bhcFrame.duration.getSelectedItem()));

        final double calculatedCost = rateForHike.getCost();

        // if the cost is positive, valid information has been given
        if (calculatedCost > 0d) {
          final NumberFormat formatter = NumberFormat.getCurrencyInstance();

          // build the calculated cost information
          final StringBuilder costNotes = new StringBuilder();
          costNotes.append("Total Cost:\t").append(formatter.format(calculatedCost))
              .append(System.lineSeparator());
          costNotes.append("Dates:\t")
              .append(rateForHike.getBeginBookingDay().toString()).append(" to ")
              .append(rateForHike.getEndBookingDay().toString())
              .append(System.lineSeparator());
          costNotes.append("Pricing Breakdown:").append(System.lineSeparator());

          // only show "normal days" pricing information if it is used
          if (rateForHike.getNormalDays() > 0) {
            costNotes.append("\t").append(rateForHike.getNormalDays()).append(" days @ ")
                .append(formatter.format(rateForHike.getBaseRate())).append(" per day")
                .append(System.lineSeparator());
          }

          // only show "premium days" pricing information if it is used
          if (rateForHike.getPremiumDays() > 0) {
            costNotes.append("\t").append(rateForHike.getPremiumDays()).append(" days @ ")
                .append(formatter.format(rateForHike.getPremiumRate())).append(" per day");
          }
          bhcFrame.tripDetailsTextArea.setText(costNotes.toString());

        } else {
          // Catch invalid input in the form of an invalid start day or an out-of-season day.
          bhcFrame.clearCalculatedInformation();
          if (rateForHike.getDetails().equals("begin or end date was out of season")) {
            final String message =
                "Please select a beginning date and duration for your hike so that the entirety\n"
                    + " of your hike starts on or after June 1 and ends on or before October 1.";
            JOptionPane.showMessageDialog(bhcFrame.mainPanel1, message,
                "Error", JOptionPane.ERROR_MESSAGE);
          } else if (rateForHike.getDetails().equals("One of the dates was not a valid day")) {
            final String message = "The chosen starting date for your hike is not a valid calendar day.";
            JOptionPane.showMessageDialog(bhcFrame.mainPanel1, message,
                "Error", JOptionPane.ERROR_MESSAGE);
          }

        }
      }
    });
  }

  /**
   * Method that clears the calculated results when the form is modified.
   *
   * @param frame
   */
  private void addClearingActionListeners(final BhcFrame frame) {
    frame.month.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.clearCalculatedInformation();
      }
    });
    frame.day.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.clearCalculatedInformation();
      }
    });
    frame.year.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.clearCalculatedInformation();
      }
    });
    frame.hikeSelection.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.clearCalculatedInformation();
      }
    });
    frame.duration.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent e) {
        frame.clearCalculatedInformation();
      }
    });
  }

  /**
   * Clears the calculated information.
   */
  private void clearCalculatedInformation() {
    this.tripDetailsTextArea.setText("");
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer >>> IMPORTANT!! <<< DO NOT edit this method OR
   * call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    mainPanel1 = new JPanel();
    mainPanel1.setLayout(
        new com.intellij.uiDesigner.core.GridLayoutManager(7, 3, new Insets(5, 5, 5, 5), -1, -1));
    mainPanel1.setMaximumSize(new Dimension(600, 600));
    mainPanel1.setMinimumSize(new Dimension(600, 250));
    mainPanel1.setOpaque(false);
    mainPanel1.setPreferredSize(new Dimension(600, 250));
    mainPanel1.setRequestFocusEnabled(false);
    monthLabel = new JLabel();
    monthLabel.setHorizontalAlignment(0);
    monthLabel.setHorizontalTextPosition(0);
    monthLabel.setText("Starting Month");
    mainPanel1.add(monthLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, -1),
        new Dimension(150, -1), null, 0, false));
    dayLabel = new JLabel();
    dayLabel.setHorizontalAlignment(0);
    dayLabel.setHorizontalTextPosition(0);
    dayLabel.setText("Starting Day");
    mainPanel1.add(dayLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    yearLabel = new JLabel();
    yearLabel.setHorizontalAlignment(0);
    yearLabel.setHorizontalTextPosition(0);
    yearLabel.setText("Starting Year");
    mainPanel1.add(yearLabel, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    month = new JComboBox();
    month.setBackground(new Color(-4473925));
    mainPanel1.add(month, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, -1),
        new Dimension(150, -1), new Dimension(150, -1), 0, false));
    day = new JComboBox();
    day.setBackground(new Color(-4473925));
    mainPanel1.add(day, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(50, -1),
        new Dimension(50, -1), new Dimension(50, -1), 0, false));
    year = new JComboBox();
    year.setBackground(new Color(-4473925));
    mainPanel1.add(year, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, -1),
        new Dimension(150, -1), new Dimension(150, -1), 0, false));
    calculateCost = new JButton();
    calculateCost.setHorizontalTextPosition(0);
    calculateCost.setText("Calculate");
    mainPanel1.add(calculateCost, new com.intellij.uiDesigner.core.GridConstraints(4, 2, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(100, -1),
        new Dimension(100, -1), new Dimension(100, -1), 0, false));
    hikesLabel = new JLabel();
    hikesLabel.setText("Possible Hikes");
    mainPanel1.add(hikesLabel, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    durationLabel = new JLabel();
    durationLabel.setHorizontalAlignment(0);
    durationLabel.setHorizontalTextPosition(0);
    durationLabel.setText("Hike Duration");
    mainPanel1.add(durationLabel, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    duration = new JComboBox();
    duration.setBackground(new Color(-4473925));
    mainPanel1.add(duration, new com.intellij.uiDesigner.core.GridConstraints(4, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(50, -1),
        new Dimension(50, -1), new Dimension(50, -1), 0, false));
    hikeSelection = new JComboBox();
    hikeSelection.setBackground(new Color(-4473925));
    mainPanel1.add(hikeSelection, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, new Dimension(150, -1),
        new Dimension(150, -1), new Dimension(150, -1), 0, false));
    tripDetailsLabel = new JLabel();
    tripDetailsLabel.setHorizontalAlignment(0);
    tripDetailsLabel.setHorizontalTextPosition(0);
    tripDetailsLabel.setText("Calculated Trip Details");
    mainPanel1.add(tripDetailsLabel, new com.intellij.uiDesigner.core.GridConstraints(5, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    bhcTitleLabel = new JLabel();
    Font bhcTitleLabelFont = this.$$$getFont$$$("Arial", -1, 16, bhcTitleLabel.getFont());
    if (bhcTitleLabelFont != null) {
      bhcTitleLabel.setFont(bhcTitleLabelFont);
    }
    bhcTitleLabel.setHorizontalAlignment(0);
    bhcTitleLabel.setHorizontalTextPosition(0);
    bhcTitleLabel.setText("BHC Trip Calculator");
    mainPanel1.add(bhcTitleLabel, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_NONE,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(173, 22),
        null, 0, false));
    final JScrollPane scrollPane1 = new JScrollPane();
    mainPanel1.add(scrollPane1, new com.intellij.uiDesigner.core.GridConstraints(6, 1, 1, 1,
        com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER,
        com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK
            | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW,
        com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK
            | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null,
        0, false));
    tripDetailsTextArea = new JTextArea();
    tripDetailsTextArea.setAutoscrolls(false);
    tripDetailsTextArea.setBackground(new Color(-4473925));
    tripDetailsTextArea.setEditable(false);
    tripDetailsTextArea.setMargin(new Insets(0, 0, 0, 0));
    tripDetailsTextArea.setMaximumSize(new Dimension(-1, -1));
    tripDetailsTextArea.setPreferredSize(new Dimension(-1, -1));
    tripDetailsTextArea.setText("");
    scrollPane1.setViewportView(tripDetailsTextArea);
    tripDetailsLabel.setLabelFor(tripDetailsTextArea);
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) {
      return null;
    }
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(),
        size >= 0 ? size : currentFont.getSize());
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return mainPanel1;
  }

}
